# Empresas Web

Este é um teste desenvolvido para a seleção na empresa Ioasys e segue os requisitos propostos. A aplicação consiste em um portal que lista empresas por meio de uma busca. Após se autenticar o usuário é redirecionado à pàgina de empresas e após pesquisar por uma empresa pode clicar no card para ver os detalhes.

## Considerações

- Estilos: utilizei a lib react-components para estilizar o projeto por causa de minha experiência com a lib, facilidade de escrever e organizar o CSS.

- Style-guide: não sendo trivial, modifiquei um pouco a proporção do style-guide pensando na melhor qualidade das UI em cada plataforma

- Componentização: Não componentizei muito o projeto, apenas demonstrei o conhecimento. A componentização deve ser feita a medida que o projeto escala e há a necessidade de reutilizar código ou se já existe uma lib de componentes. Apesar disso, muito do projeto já poderia ser refatorado (fica pra próxima iteração)

- Testes: Foram feitos apenas alguns testes na aplicação, pensando em testes de integração e no comportamento das páginas. Devem ser adicionados mais testes.

- Logout: Adicionei o logout na aplicação para deixar a aplicação mais usável, no canto inferior direito.

- Gerenciamento de estados: pelo tamanho da aplicação e pouca necessidade de gerenciamento de estados optei pelo Context API ao invés de Flux.

## Run a aplicação

Para rodar a aplicação é necessário clonar o projeto e seguir os comandos abaixo:

```
//clonar o projeto
git clone https://agnaldoburgojr@bitbucket.org/agnaldoburgojr/empresas-web.git empresas

//entrar no diretório
cd empresas

//rodar o projeto
yarn install && yarn start
```

A aplicação estará disponível em [http://localhost:3000](http://localhost:3000)

### Testes

Para rodar os testes use o comando dentro do diretório do projeto

```
yarn test
```

## Contato

Fico a disposição para explicação e dúvidas sobre o projeto.

- Email: agnaldoburgojr@gmail.com
- Cel: (14) 99625-7952

---

Feito com ♥ por Agnaldo Burgo Junior :wave: [Get in touch!](https://www.linkedin.com/in/agnaldo-burgo-junior/)
