import api from './api'

const get = (params) => {
  return api.get('enterprises', { params })
}

const show = (enterpriseId) => {
  return api.get(`enterprises/${enterpriseId}`)
}

export default { get, show }
