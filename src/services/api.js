import axios from 'axios'
import config from '../config'

const api = axios.create({
  baseURL: `${config.base_url}/api/v1`,
  headers: { 'Content-Type': 'application/json' },
})

export default api
