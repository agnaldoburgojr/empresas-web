import React from 'react'

import { Container } from './styles'

const Spinner = () => {
  return (
    <Container data-testid='spinner'>
      <div />
      <div />
      <div />
      <div />
    </Container>
  )
}

export default Spinner
