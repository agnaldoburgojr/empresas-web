import React, { useCallback } from 'react'
import { VscSignOut } from 'react-icons/vsc'
import { useHistory } from 'react-router-dom'
import { useAuth } from '../../hooks/auth'
import pathnames from '../../routes/pathnames'
import { Container } from './styles'

const SignoutButton = () => {
  const history = useHistory()
  const { signOut } = useAuth()

  const logout = useCallback(() => {
    signOut()
    history.push(pathnames.login)
  }, [history, signOut])

  return (
    <Container>
      <VscSignOut onClick={logout} data-testid='logoutButton' />
    </Container>
  )
}

export default SignoutButton
