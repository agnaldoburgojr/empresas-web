import styled from 'styled-components'
import colors from '../../styles/colors'

export const Container = styled.div`
  position: fixed;
  bottom: 24px;
  right: 24px;
  z-index: 10;

  svg {
    font-size: 2rem;
    color: ${colors.warmGrey};
    opacity: 0.8;
    cursor: pointer;
  }
`
