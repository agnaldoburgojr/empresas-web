import React, { createContext, useState, useContext } from 'react'

const EnterprisesContext = createContext({})

const EnterpriseProvider = ({ children }) => {
  const [enterprises, setEnterprises] = useState([])

  return (
    <EnterprisesContext.Provider value={{ enterprises, setEnterprises }}>
      {children}
    </EnterprisesContext.Provider>
  )
}

function useEnterprises() {
  const context = useContext(EnterprisesContext)

  if (!context) {
    throw new Error('userAuth must be used within an EnterpriseProvider')
  }

  return context
}

export { EnterpriseProvider, useEnterprises }
