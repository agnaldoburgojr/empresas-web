import React, { createContext, useCallback, useState, useContext } from 'react'
import api from '../services/api'

const AuthContext = createContext({})

const PROJECT_KEY = '@ioasys-empresas'

const AuthProvider = ({ children }) => {
  const setHeaders = ({ accessToken, client, uid }) => {
    api.defaults.headers['access-token'] = accessToken
    api.defaults.headers.client = client
    api.defaults.headers.uid = uid
  }

  const [data, setData] = useState(() => {
    const userString = localStorage.getItem(`${PROJECT_KEY}:user`)

    if (userString) {
      const user = JSON.parse(userString)
      setHeaders(user)
      return { user }
    }
    return {}
  })

  const signIn = useCallback(async ({ email, password }) => {
    const response = await api.post('users/auth/sign_in', { email, password })

    const accessToken = response.headers['access-token']
    const { client, uid } = response.headers

    const user = { accessToken, client, uid }

    localStorage.setItem(`${PROJECT_KEY}:user`, JSON.stringify(user))
    setHeaders(user)
    setData({ user })
  }, [])

  const signOut = useCallback(() => {
    localStorage.removeItem(`${PROJECT_KEY}:user`)
    setData({})
  }, [])

  return (
    <AuthContext.Provider value={{ user: data.user, signIn, signOut }}>
      {children}
    </AuthContext.Provider>
  )
}

function useAuth() {
  const context = useContext(AuthContext)

  if (!context) {
    throw new Error('userAuth must be used within an AuthProvider')
  }

  return context
}

export { AuthProvider, useAuth }
