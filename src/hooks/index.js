import React from 'react'

import { AuthProvider } from './auth'
import { EnterpriseProvider } from './enterprises'

const AppProvider = ({ children }) => (
  <AuthProvider>
    <EnterpriseProvider>{children}</EnterpriseProvider>
  </AuthProvider>
)

export default AppProvider
