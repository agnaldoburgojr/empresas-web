import React from 'react'
import { render } from '@testing-library/react'
import { Spinner } from '../../components'

describe('Spinner', () => {
  it('should have four divs to render correctly', () => {
    const { getByTestId } = render(<Spinner />)
    const spinner = getByTestId('spinner')

    expect(spinner.childElementCount).toBe(4)
  })
})
