import React from 'react'
import { fireEvent, render, waitFor } from '@testing-library/react'
import { SignoutButton } from '../../components'

const mockedHistoryPush = jest.fn()
const mockedSignout = jest.fn()

jest.mock('react-router-dom', () => {
  return {
    useHistory: () => ({
      push: mockedHistoryPush,
    }),
  }
})

jest.mock('../../hooks/auth', () => {
  return {
    useAuth: () => ({
      signOut: mockedSignout,
    }),
  }
})

describe('SignoutButton', () => {
  beforeEach(() => {
    mockedHistoryPush.mockClear()
  })

  it('should goes to login when click on signout button', async () => {
    const { getByTestId } = render(<SignoutButton />)
    const logoutButton = getByTestId('logoutButton')
    fireEvent.click(logoutButton)

    await waitFor(() =>
      expect(mockedHistoryPush).toHaveBeenCalledWith('/login')
    )
  })
})
