import React from 'react'
import { render, fireEvent, waitFor } from '@testing-library/react'
import { Details } from '../../pages'

const mockedHistoryGoBack = jest.fn()

jest.mock('react-router-dom', () => {
  return {
    useHistory: () => ({
      goBack: mockedHistoryGoBack,
    }),
    useLocation: () => ({
      state: {
        enterprise_name: 'name',
        description: 'lorem ipsum',
        photo: 'any_url',
      },
      pathname: '/details/2',
    }),
  }
})

describe('Details Page', () => {
  it('should starts with initial state', () => {
    const { getByTestId } = render(<Details />)
    const title = getByTestId('detailsTitle')
    const description = getByTestId('detailsDescripition')

    expect(title.textContent).toBe('name')
    expect(description.textContent).toBe('lorem ipsum')
  })

  it('should goes to home when click in return button', async () => {
    const { getByTestId } = render(<Details />)
    const goBackButton = getByTestId('goBackButton')

    fireEvent.click(goBackButton)

    await waitFor(() => expect(mockedHistoryGoBack).toHaveBeenCalled())
  })
})
