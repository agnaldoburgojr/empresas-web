import React from 'react'
import { fireEvent, render, waitFor } from '@testing-library/react'
import { Login } from '../../pages'

const mockedHistoryPush = jest.fn()
const mockedLogin = jest.fn()

jest.mock('react-router-dom', () => {
  return {
    useHistory: () => ({
      push: mockedHistoryPush,
    }),
  }
})

jest.mock('../../hooks/auth', () => {
  return {
    useAuth: () => ({
      signIn: mockedLogin,
    }),
  }
})

describe('Login Page', () => {
  beforeEach(() => {
    mockedHistoryPush.mockClear()
  })

  it('should starts with initial state', () => {
    const { getByText, getByTestId, getByPlaceholderText } = render(<Login />)
    const loginButton = getByText('Entrar')
    const errorMessage = getByTestId('errorMessage')
    const passwordField = getByPlaceholderText('Senha')
    expect(loginButton).toBeDisabled()
    expect(errorMessage.textContent).toBeFalsy()
    expect(passwordField.getAttribute('type')).toBe('password')
  })

  it('should be able to sign in', async () => {
    const { getByPlaceholderText, getByText } = render(<Login />)

    const emailField = getByPlaceholderText('E-mail')
    const passwordField = getByPlaceholderText('Senha')
    const buttonElement = getByText('Entrar')

    fireEvent.change(emailField, { target: { value: 'johndoe@example.com' } })
    fireEvent.change(passwordField, { target: { value: '123456' } })

    fireEvent.click(buttonElement)

    await waitFor(() =>
      expect(mockedHistoryPush).toHaveBeenCalledWith('/empresas')
    )
  })

  it('should not be able to sign in with invalid credentials', async () => {
    const { getByPlaceholderText, getByText } = render(<Login />)

    const emailField = getByPlaceholderText('E-mail')
    const passwordField = getByPlaceholderText('Senha')
    const buttonElement = getByText('Entrar')

    fireEvent.change(emailField, { target: { value: 'not-valid-email' } })
    fireEvent.change(passwordField, { target: { value: '123456' } })

    fireEvent.click(buttonElement)

    await waitFor(() => {
      expect(mockedHistoryPush).not.toHaveBeenCalled()
    })
  })
})
