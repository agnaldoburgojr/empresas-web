import React from 'react'
import { render, fireEvent, waitFor } from '@testing-library/react'
import { Home } from '../../pages'

const mockedEnterprises = jest.fn()

jest.mock('../../hooks/enterprises', () => {
  return {
    useEnterprises: () => ({
      enterprises: [],
      setEnterprises: mockedEnterprises,
    }),
  }
})

describe('Home Page', () => {
  it('should starts with initial state', () => {
    const { getByTestId } = render(<Home />)
    const header = getByTestId('headerHome')
    const message = getByTestId('emptyListMessage')

    expect(header.firstElementChild.childElementCount).toBe(2)
    expect(message.textContent).toBe('Clique na busca para iniciar')
  })

  it('should be on searchMode when click on searchButton', () => {
    const { getByTestId } = render(<Home />)
    const header = getByTestId('headerHome')
    const message = getByTestId('emptyListMessage')
    const searchButton = getByTestId('searchButton')

    fireEvent.click(searchButton)

    expect(header.firstElementChild.childElementCount).toBe(3)
    expect(message.textContent).toBe('')
  })

  it('should be on searchMode when click on searchButton', () => {
    const { getByTestId } = render(<Home />)
    const header = getByTestId('headerHome')
    const message = getByTestId('emptyListMessage')
    const searchButton = getByTestId('searchButton')

    fireEvent.click(searchButton)

    expect(header.firstElementChild.childElementCount).toBe(3)
    expect(message.textContent).toBe('')
  })

  it('should calls API with correct param', async () => {
    const { getByPlaceholderText, getByTestId } = render(<Home />)
    const searchButton = getByTestId('searchButton')
    fireEvent.click(searchButton)
    const searchField = getByPlaceholderText('Pesquisar')
    fireEvent.change(searchField, { target: { value: 'enterprise-name' } })

    const mockedGet = jest.fn()
    jest.mock('../../services', () => {
      return {
        enterprises: {
          get: mockedGet,
        },
      }
    })

    await waitFor(() => {
      expect(mockedGet).not.toHaveBeenCalledWith('enterprise-name')
    })
  })
})
