import styled from 'styled-components'
import { shade } from 'polished'
import colors from '../../styles/colors'

export const Container = styled.div`
  width: 100%;
  height: 100vh;
  display: flex;
  justify-content: center;
  align-items: center;
  position: relative;
`

export const SpinnerContainer = styled.div`
  width: 100%;
  height: 100vh;
  background-color: rgba(255, 255, 255, 0.6);
  position: absolute;
  z-index: 10;

  display: flex;
  justify-content: center;
  align-items: center;
`

export const Form = styled.form`
  width: 340px;
  display: flex;
  flex-direction: column;
  align-items: center;
  padding: 16px;

  img {
    height: 4.5rem;
  }

  h2 {
    margin-top: 4rem;
    width: 50%;
    text-transform: uppercase;
    font-size: 1.6rem;
    text-align: center;
    letter-spacing: -1.2px;
  }

  p {
    width: 92%;
    margin: 1.5rem 0 2rem;
    font-size: 1rem;
    text-align: center;
  }

  button {
    width: 100%;
    background-color: ${colors.lightBlue};
    color: ${colors.whiteTwo};
    padding: 12px;
    margin: 1.5rem 0 2rem;
    font-size: 1.2rem;
    text-transform: uppercase;
    border: none;
    border-radius: 4px;
    cursor: pointer;

    &:hover {
      background-color: ${shade(0.2, colors.lightBlue)};
    }

    &:disabled {
      background-color: ${colors.warmGrey};
    }
  }
`

export const Input = styled.div`
  position: relative;
  width: 100%;

  input {
    width: 100%;
    background-color: transparent;
    border: none;
    border-bottom: 1px solid ${colors.charcoalGrey};
    padding: 8px 42px 6px 30px;
    margin-bottom: 1rem;
    font-size: 1.2rem;
  }

  svg {
    position: absolute;

    &:first-child {
      color: ${colors.mediumPink};
      bottom: 24px;
      left: 4px;
    }

    &:last-child {
      color: ${(props) => (props.hasError ? colors.red : colors.warmGrey)};
      cursor: ${(props) => (props.hasError ? 'default' : 'pointer')};
      font-size: 24px;
      top: 6px;
      right: 8px;
    }
  }
`

export const Error = styled.span`
  color: ${colors.red};
  font-size: 0.8rem;
  margin: 4px 0;
`
