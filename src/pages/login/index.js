import React, { useState, useCallback } from 'react'
import { useHistory } from 'react-router-dom'
import { HiOutlineMail, HiOutlineLockOpen } from 'react-icons/hi'
import { MdVisibility, MdVisibilityOff, MdError } from 'react-icons/md'
import { useAuth } from '../../hooks/auth'
import { Spinner } from '../../components'
import pathnames from '../../routes/pathnames'
import { Container, Form, Input, SpinnerContainer, Error } from './styles'
import Logo from '../../assets/logo-home.png'

const Login = () => {
  const [isLoading, setLoading] = useState(false)
  const [errorMessage, setErrorMessage] = useState('')
  const [isVisible, setVisible] = useState(false)
  const [loginData, setLoginData] = useState({ email: '', password: '' })

  const { signIn } = useAuth()
  const history = useHistory()

  const handleVisibility = useCallback(() => {
    setVisible(!isVisible)
  }, [isVisible])

  const handleChange = useCallback(
    (event) => {
      setLoginData({ ...loginData, [event.target.name]: event.target.value })
      setErrorMessage('')
    },
    [loginData]
  )

  const handleSubmit = useCallback(
    async (event) => {
      event.preventDefault()
      setLoading(true)
      try {
        await signIn({ email: loginData.email, password: loginData.password })
        setLoading(false)
        history.push(pathnames.enterprise)
      } catch (error) {
        setLoading(false)
        setErrorMessage(
          'Credenciais informadas são inválidas, tente novamente.'
        )
      }
    },
    [signIn, history, loginData]
  )

  const getIcon = (hasError, hasVisibility) => {
    if (hasError) return <MdError />

    return hasVisibility ? (
      <MdVisibilityOff onClick={handleVisibility} />
    ) : (
      <MdVisibility onClick={handleVisibility} />
    )
  }

  return (
    <Container>
      {isLoading && (
        <SpinnerContainer>
          <Spinner />
        </SpinnerContainer>
      )}
      <Form onSubmit={handleSubmit}>
        <img src={Logo} alt='Logotipo Ioasys' />
        <h2>Bem vindo ao empresas</h2>
        <p>
          Lorem ipsum dolor sit amet, contetur adipiscing eli. Nunc accumsan.
        </p>
        <Input hasError={!!errorMessage}>
          <HiOutlineMail />
          <input
            type='text'
            placeholder='E-mail'
            name='email'
            value={loginData.email}
            onChange={handleChange}
          />
          {errorMessage && <MdError hasError />}
        </Input>

        <Input hasError={!!errorMessage}>
          <HiOutlineLockOpen />
          <input
            type={isVisible ? 'text' : 'password'}
            placeholder='Senha'
            name='password'
            value={loginData.password}
            onChange={handleChange}
          />
          {getIcon(errorMessage, isVisible)}
        </Input>
        <Error data-testid='errorMessage'>{errorMessage}</Error>
        <button
          type='submit'
          disabled={errorMessage || !loginData.email || !loginData.password}
        >
          Entrar
        </button>
      </Form>
    </Container>
  )
}

export default Login
