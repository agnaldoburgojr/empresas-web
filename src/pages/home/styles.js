import styled from 'styled-components'
import { shade } from 'polished'
import colors from '../../styles/colors'

export const Container = styled.div`
  width: 100%;
  display: flex;
  flex-direction: column;
`

export const Header = styled.header`
  width: 100%;
  height: 7rem;
  background: ${colors.mediumPink};
  background: linear-gradient(
    180deg,
    ${colors.mediumPink} 0%,
    ${shade(0.2, colors.mediumPink)} 100%
  );
  display: flex;
  justify-content: center;
  align-items: center;
  padding: 16px;

  div {
    max-width: 700px;
    width: 100%;
    height: 100%;
    display: flex;
    justify-content: center;
    align-items: center;
    position: relative;

    img {
      height: 2.5rem;
    }

    svg {
      position: absolute;
      right: 0;
      margin-top: 4px;
      margin-right: 16px;
      font-size: 1.8rem;
      color: ${colors.whiteTwo};
      cursor: pointer;
    }
  }
`

export const EmptyList = styled.div`
  flex-grow: 1;
  display: flex;
  justify-content: center;
  align-items: center;
  height: calc(100vh - 7rem);

  p {
    text-align: center;
    font-size: 1.4rem;
  }
`

export const Input = styled.div`
  position: relative;
  width: 100%;

  input {
    width: 100%;
    background-color: transparent;
    border: none;
    border-bottom: 1px solid ${colors.whiteTwo};
    padding: 8px 42px 6px 42px;
    margin-bottom: 1rem;
    font-size: 1.2rem;
    color: ${colors.whiteTwo};

    &::placeholder {
      color: #991237;
    }
  }

  svg {
    position: absolute;
    font-size: 2.4rem;

    &:first-child {
      cursor: default;
      bottom: 32px;
      left: 4px;
    }

    &:last-child {
      font-size: 24px;
      top: 12px;
      right: -8px;
    }
  }
`

export const Content = styled.div`
  margin: 0 auto;
  max-width: 700px;
  width: 100%;
  padding-top: 24px;

  a {
    text-decoration: none;
  }
`
export const Card = styled.div`
  background-color: ${colors.whiteTwo};
  padding: 24px;
  display: flex;
  border-radius: 4px;
  margin-bottom: 16px;

  div:first-child {
    width: 200px;
    height: 110px;
    background-color: ${colors.charcoalGrey};

    img {
      width: 200px;
      object-fit: cover;
    }
  }

  div + div {
    flex-grow: 1;
    margin-left: 24px;
    color: ${colors.warmGrey};
    display: flex;
    flex-direction: column;
    justify-content: center;

    h2 {
      color: ${colors.darkIndigo};
      margin-bottom: 4px;
    }

    h3 {
      font-weight: 400;
      margin-bottom: 4px;
    }
  }
`
