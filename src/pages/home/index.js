import React, { useState, useCallback, useRef, useEffect, useMemo } from 'react'
import { Link, useHistory } from 'react-router-dom'
import { debounce } from 'lodash'
import { HiSearch } from 'react-icons/hi'
import { MdClose } from 'react-icons/md'
import * as services from '../../services'
import { useAuth } from '../../hooks/auth'
import { useEnterprises } from '../../hooks/enterprises'
import config from '../../config'
import pathnames from '../../routes/pathnames'
import { SignoutButton } from '../../components'
import { Container, Header, EmptyList, Content, Input, Card } from './styles'
import Logo from '../../assets/logo-nav.png'

const Home = () => {
  const [isSearchMode, setSearchMode] = useState(false)
  const [search, setSearch] = useState('')
  const { enterprises, setEnterprises } = useEnterprises()
  const [isFetchDone, setFetchDone] = useState(false)
  const { signOut } = useAuth()
  const history = useHistory()

  useEffect(() => {
    if (search) {
      services.enterprises
        .get({ name: search })
        .then((response) => {
          setEnterprises(response.data.enterprises)
          if (!response.data.enterprise.length) setFetchDone(true)
        })
        .catch((error) => {
          if (error.message === 'Request failed with status code 401') {
            setEnterprises([])
            signOut()
            history.replace(pathnames.login)
          }
          setFetchDone(true)
        })
    }
  }, [search, signOut, history])

  const delayedQuery = useRef(
    debounce(
      (value) => {
        setSearch(value)
      },
      [800]
    )
  ).current

  const handleChange = useCallback((event) => {
    delayedQuery(event.target.value)
  }, [])

  const handleSearchMode = useCallback(() => {
    setSearchMode(!isSearchMode)
    setFetchDone(false)
  }, [isSearchMode])

  const getMessageToEmptyList = useMemo(() => {
    if (!isSearchMode) return 'Clique na busca para iniciar'
    if (!isFetchDone) return ''
    return 'Nenhuma empresa foi encontrada para a busca realizada.'
  }, [isSearchMode, isFetchDone])

  return (
    <Container>
      <Header data-testid='headerHome'>
        {!isSearchMode ? (
          <div>
            <img src={Logo} alt='Logotipo Ioasys' />
            <HiSearch onClick={handleSearchMode} data-testid='searchButton' />
          </div>
        ) : (
          <Input>
            <HiSearch />
            <input
              type='text'
              placeholder='Pesquisar'
              onChange={handleChange}
            />
            <MdClose onClick={handleSearchMode} />
          </Input>
        )}
      </Header>
      {enterprises.length === 0 ? (
        <EmptyList>
          <p data-testid='emptyListMessage'>{getMessageToEmptyList}</p>
        </EmptyList>
      ) : (
        <Content>
          {enterprises.map((enterprise) => (
            <Link
              to={{
                pathname: `empresas/${enterprise.id}`,
                state: {
                  enterprise_name: enterprise.enterprise_name,
                  photo: enterprise.photo,
                  description: enterprise.description,
                },
              }}
              key={enterprise.id}
            >
              <Card>
                <div>
                  <img
                    src={`${config.base_url}${enterprise.photo}`}
                    alt={enterprise.enterprise_name}
                  />
                </div>
                <div>
                  <h2>{enterprise.enterprise_name}</h2>
                  <h3>{enterprise.enterprise_type.enterprise_type_name}</h3>
                  <span>
                    {enterprise.city} - {enterprise.country}
                  </span>
                </div>
              </Card>
            </Link>
          ))}
        </Content>
      )}
      <SignoutButton />
    </Container>
  )
}

export default Home
