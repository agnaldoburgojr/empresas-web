import React, { useCallback, useState, useEffect } from 'react'
import { useHistory, useLocation } from 'react-router-dom'
import { HiArrowLeft } from 'react-icons/hi'
import * as services from '../../services'
import { useAuth } from '../../hooks/auth'
import config from '../../config'
import pathnames from '../../routes/pathnames'
import { Container, Header, Content, Card } from './styles'
import { SignoutButton } from '../../components'

function Details() {
  const history = useHistory()
  const { signOut } = useAuth()
  const { state, pathname } = useLocation()
  const enterpriseId = pathname.substr(9, pathname.length - 1)
  const [enterprise, setEnterprise] = useState(() => {
    return state !== undefined
      ? state
      : { enterprise_name: '', photo: '', description: '' }
  })

  useEffect(() => {
    if (state === undefined) {
      services.enterprises
        .show(enterpriseId)
        .then((response) => {
          setEnterprise(response.data.enterprise)
        })
        .catch((error) => {
          if (error.message === 'Request failed with status code 401') {
            signOut()
            history.replace(pathnames.login)
          }
        })
    }
  }, [signOut, history, state, enterpriseId])

  const handleGoBack = useCallback(() => {
    history.goBack()
  }, [history])

  return (
    <Container>
      <Header>
        <div>
          <HiArrowLeft onClick={handleGoBack} data-testid='goBackButton' />
          <h2 data-testid='detailsTitle'>{enterprise.enterprise_name}</h2>
        </div>
      </Header>
      <Content>
        <Card>
          <div>
            <img
              src={`${config.base_url}${enterprise.photo}`}
              alt={enterprise.name}
            />
          </div>
          <p data-testid='detailsDescripition'>{enterprise.description}</p>
        </Card>
      </Content>
      <SignoutButton />
    </Container>
  )
}

export default Details
