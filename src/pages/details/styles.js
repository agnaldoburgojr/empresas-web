import styled from 'styled-components'
import { shade } from 'polished'
import colors from '../../styles/colors'

export const Container = styled.div`
  width: 100%;
  display: flex;
  flex-direction: column;
`

export const Header = styled.header`
  width: 100%;
  height: 7rem;
  background: ${colors.mediumPink};
  background: linear-gradient(
    180deg,
    ${colors.mediumPink} 0%,
    ${shade(0.2, colors.mediumPink)} 100%
  );
  display: flex;
  justify-content: center;
  align-items: center;
  padding: 16px;

  div {
    max-width: 700px;
    width: 100%;
    height: 100%;
    display: flex;
    align-items: center;

    h2 {
      color: ${colors.whiteTwo};
      margin-left: 24px;
    }

    svg {
      font-size: 1.8rem;
      color: ${colors.whiteTwo};
      cursor: pointer;
    }
  }
`

export const Content = styled.div`
  margin: 0 auto;
  max-width: 700px;
  width: 100%;
  padding-top: 24px;
`
export const Card = styled.div`
  background-color: ${colors.whiteTwo};
  padding: 24px;
  display: flex;
  flex-direction: column;
  border-radius: 4px;

  div {
    width: 100%;
    height: 180px;
    background-color: ${colors.charcoalGrey};

    img {
      width: 100%;
      height: 180px;
      object-fit: cover;
    }
  }

  p {
    margin-top: 24px;
    text-align: justify;
    font-size: 1.4rem;
    color: ${colors.warmGrey};
  }

  div + div {
    flex-grow: 1;
    margin-left: 24px;
    color: ${colors.warmGrey};
    display: flex;
    flex-direction: column;
    justify-content: center;

    h2 {
      color: ${colors.darkIndigo};
      margin-bottom: 4px;
    }

    h3 {
      font-weight: 400;
      margin-bottom: 4px;
    }
  }
`
