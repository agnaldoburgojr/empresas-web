import React from 'react'
import { Switch } from 'react-router-dom'

import Route from './Route'
import pathnames from './pathnames'
import { Login, Home, Details } from '../pages'

const Routes = () => {
  return (
    <Switch>
      <Route exact path={pathnames.login} component={Login} />
      <Route exact path={pathnames.enterprise} component={Home} isPrivate />
      <Route
        exact
        path={pathnames.enterpriseDetails}
        component={Details}
        isPrivate
      />
      <Route path='/' component={Home} isPrivate />
    </Switch>
  )
}

export default Routes
