export default {
  login: '/login',
  enterprise: '/empresas',
  enterpriseDetails: '/empresas/:id',
}
