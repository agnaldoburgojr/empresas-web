import React from 'react'
import { Route as ReactDOMRoute, Redirect } from 'react-router-dom'
import { useAuth } from '../hooks/auth'
import pathnames from './pathnames'

const Route = ({ isPrivate = false, component: Component, ...rest }) => {
  const { user } = useAuth()

  return (
    <ReactDOMRoute
      {...rest}
      render={({ location }) => {
        return isPrivate === !!user ? (
          <Component />
        ) : (
          <Redirect
            to={{
              pathname: isPrivate ? pathnames.login : pathnames.enterprise,
              state: { from: location },
            }}
          />
        )
      }}
    />
  )
}

export default Route
