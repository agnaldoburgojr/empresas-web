export default {
  charcoalGrey: '#383743',
  warmGrey: '#8d8c8c',
  mediumPink: '#ee4c77',
  whiteTwo: '#fff',
  red: '#ff0f44',
  lightBlue: '#57bbbc',
  darkIndigo: '#1a0e49',
  background: '#eeecdb',
}
