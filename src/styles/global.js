import { createGlobalStyle } from 'styled-components'
import colors from './colors'

export default createGlobalStyle`
  * {
    margin: 0;
    padding: 0;
    box-sizing: border-box;
    outline: 0;
   
  }
  
  html {
    font-size: 14px; //Tamanho da fonte na raiz
  }

  body {
    background: ${colors.background};
    color: ${colors.charcoalGrey};
    font-family: 'Roboto', sans-serif;
    -webkit-font-smoothing: antialiased;
  }

  input, button {
    font-family: 'Roboto', sans-serif;
  }

  h1, h2,h3, h4, h5, h6, strong {
    font-weight: 700;
  }

  button: {
    cursor: pointer;
  }

`
